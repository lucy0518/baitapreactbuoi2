import React, { Component } from "react";
import ItemShoe from "./ItemShoe";

export default class ListShoe extends Component {
  render() {
    return (
      <div className="container">
        <div className=" row">
          {this.props.data.map((item, index) => {
            return (
              <ItemShoe
                handleViewDetail={this.props.handleXemChiTiet}
                detail={item}
                handleAddToCart={this.props.handleAddToCart}
              />
            );
          })}
        </div>
      </div>
    );
  }
}
